
uniform vec4 g_LightPosition;

varying vec3 vPosition;
varying vec3 vNormal;


//Horizon and zenith colours
uniform vec4 m_horizonColor;
uniform vec4 m_zenithColor;

uniform float m_sunScale;
uniform float m_domeRadius;

void main() {

    //Finish z position to be configurable
    float fac = clamp(0.0, 1.0, abs(vPosition.y)/m_domeRadius);

    vec4 skyColor = mix(m_horizonColor, m_zenithColor, fac);

    vec3 sunTexel = (normalize(-g_LightPosition.xyz))*m_domeRadius;

    //Set a radius around the sun texel to be white (sun)
    //Glow effect for radius higher than the sun (inverse square falloff)
    float sunFac = clamp(0.0, 1.0, (m_sunScale*64.0)/(pow(distance(sunTexel, vPosition), 2.0)));

    //float sunFac = max(0.0, pow(dot(vNormal, normalize(g_LightPosition.xyz)), 9));
    vec4 sunColor = mix(skyColor, vec4(1.0, 1.0, 1.0, 1.0), sunFac);

    gl_FragColor = sunColor;
}
