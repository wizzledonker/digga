#import "Common/ShaderLib/Lighting.glsllib"

uniform mat4 g_WorldViewProjectionMatrix;
uniform mat4 g_WorldViewMatrix;

attribute vec3 inPosition;
attribute vec3 inNormal;

varying vec3 vPosition;
varying vec3 vNormal;

void main(){
    vec4 pos = vec4(inPosition, 1.0);

    vPosition = inPosition;
    vNormal = inNormal;

    gl_Position = g_WorldViewProjectionMatrix * pos;
}