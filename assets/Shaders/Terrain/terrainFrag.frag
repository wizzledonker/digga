#import "Common/ShaderLib/BlinnPhongLighting.glsllib"
#import "Common/ShaderLib/Lighting.glsllib"

uniform float Shininess;
uniform vec4 g_LightDirection;

varying vec4 AmbientSum;
varying vec4 DiffuseSum;
varying vec4 SpecularSum;

varying vec3 vNormal;
varying vec2 texCoord;
varying vec3 vPosition;
varying vec3 vViewDir;
varying vec4 vLightDir;
varying vec3 lightVec;

#ifdef DIFFUSEMAP
    uniform float m_BlendRange;
    uniform sampler2D m_DiffuseMap;
    uniform float m_DiffuseMap_0_scale;
#endif

#ifdef DIFFUSEMAP_SLOPE
  uniform sampler2D m_DiffuseMap_slope;
  uniform float m_DiffuseMap_slope_scale;
  uniform float m_DiffuseMap_slope_vscale;
#endif
#ifdef DIFFUSEMAP_2
  uniform sampler2D m_DiffuseMap_2;
  uniform float m_DiffuseMap_2_min;
  uniform float m_DiffuseMap_2_max;
  uniform float m_DiffuseMap_2_scale;
#endif
#ifdef DIFFUSEMAP_3
  uniform sampler2D m_DiffuseMap_3;
  uniform float m_DiffuseMap_3_min;
  uniform float m_DiffuseMap_3_max;
  uniform float m_DiffuseMap_3_scale;
#endif

//Removed the alpha map section
#ifdef NORMALMAP
  uniform sampler2D m_NormalMap;
#endif
#ifdef NORMALMAP_SLOPE
  uniform sampler2D m_NormalMap_slope;
#endif
#ifdef NORMALMAP_2
  uniform sampler2D m_NormalMap_2;
#endif
#ifdef NORMALMAP_3
  uniform sampler2D m_NormalMap_3;
#endif

varying vec4 wVertex;
varying vec3 wNormal;

float getIntensityForHeight(in float height, in float max, in float min) {
    float regionWeight;
    //Check base level intensity
    if (height > min && height < max) {
        regionWeight = 1.0;
    } else {
        //Third Texture
        if (height < min) {
            regionWeight = abs(height-min); // Get difference between height and the max position of diffuse map but only if it's negative
            regionWeight = clamp(0.0, 1.0, (m_BlendRange-regionWeight)/m_BlendRange);
        }

        if (height > max) {
            regionWeight = abs(height-max); // Get difference between height and the max position of diffuse map but only if it's negative
            regionWeight = clamp(0.0, 1.0, (m_BlendRange-regionWeight)/m_BlendRange);
        }
    }
    return regionWeight;
}

vec4 calculateTriPlanarDiffuseBlend(in vec3 wNorm, in vec4 wVert) {
    // Texture blending for slopes and in-between normals
    vec3 blending = abs( wNorm );
    blending = (blending -0.2) * 0.7;
    blending = normalize(max(blending, 0.00001));      // Force weights to sum to 1.0 (very important!)
    float b = (blending.x + blending.y + blending.z);
    blending /= vec3(b, b, b);

    // texture coords
    vec4 coords = wVert;

    float height = wVert.y;

    // blend the results of the 3 planar projections.
    vec4 tex0 = texture2D(m_DiffuseMap, coords.xz * m_DiffuseMap_0_scale);
    //vec4 tex0 = getTriPlanarBlend(coords, blending, DiffuseMap, 0.1);

    #ifdef DIFFUSEMAP_SLOPE
      // blend the results of the 3 planar projections.
      vec4 tex1_x = texture2D(m_DiffuseMap_slope, vec2(coords.x * m_DiffuseMap_slope_scale, coords.y*m_DiffuseMap_slope_vscale));
      vec4 tex1_z = texture2D(m_DiffuseMap_slope, vec2(coords.z * m_DiffuseMap_slope_scale, coords.y*m_DiffuseMap_slope_vscale));
    #endif
    #ifdef DIFFUSEMAP_2
      // blend the results of the 3 planar projections.
      vec4 tex2 = texture2D(m_DiffuseMap_2, coords.xz * m_DiffuseMap_2_scale);
    #endif
    #ifdef DIFFUSEMAP_3
      // blend the results of the 3 planar projections.
      vec4 tex3 = texture2D(m_DiffuseMap_3, coords.xz * m_DiffuseMap_3_scale);
    #endif

    vec4 diffuseColor = tex0;

    float currWeight = 0.0;

    #ifdef DIFFUSEMAP_2
        currWeight = getIntensityForHeight(height, m_DiffuseMap_2_max, m_DiffuseMap_2_min);

        diffuseColor = mix(diffuseColor, tex2,  currWeight);
    #endif
    #ifdef DIFFUSEMAP_3
        //Fourth Texture
        currWeight = getIntensityForHeight(height, m_DiffuseMap_3_max, m_DiffuseMap_3_min);

        diffuseColor = mix(diffuseColor, tex3,  currWeight);
    #endif

    #ifdef DIFFUSEMAP_SLOPE
        diffuseColor = blending.y * diffuseColor + blending.x * tex1_z + blending.z * tex1_x;
    #endif

    return diffuseColor;
}

vec3 calculateNormalTriPlanar(in vec3 wNorm, in vec4 wVert) {
    // tri-planar texture bending factor for this fragment's world-space normal
    vec3 blending = abs( wNorm );
    blending = (blending -0.2) * 0.7;
    blending = normalize(max(blending, 0.00001));      // Force weights to sum to 1.0 (very important!)
    float b = (blending.x + blending.y + blending.z);
    blending /= vec3(b, b, b);

    // texture coords
    vec4 coords = wVert;
    float height = vPosition.y;

    vec3 normal = vec3(0,0,1);
    vec3 n = vec3(0,0,0);

    //First Texture
    #ifdef DIFFUSEMAP
        #ifdef NORMALMAP
            n = texture2D(m_NormalMap, coords.xz*m_DiffuseMap_0_scale);
            normal += n;
        #else
            normal += vec3(0.5,0.5,1);
        #endif
    #endif

    #ifdef DIFFUSEMAP_2

        #ifdef NORMALMAP
            n = texture2D(m_NormalMap_2, coords.xz*m_DiffuseMap_2_scale);
            normal += n * getIntensityForHeight(height, m_DiffuseMap_2_max, m_DiffuseMap_2_min);
        #else
            normal += vec3(0.5,0.5,1) * getIntensityForHeight(height, m_DiffuseMap_2_max, m_DiffuseMap_2_min);;
        #endif
    #endif

    #ifdef DIFFUSEMAP_3
        #ifdef NORMALMAP_3
            n = texture2D(m_NormalMap_3, coords.xz*m_DiffuseMap_3_scale);
            normal += n * getIntensityForHeight(height, m_DiffuseMap_3_max, m_DiffuseMap_3_min);
        #else
            normal += vec3(0.5,0.5,1) * getIntensityForHeight(height, m_DiffuseMap_3_max, m_DiffuseMap_3_min);;
        #endif
    #endif

    #ifdef DIFFUSEMAP_SLOPE
        //Slope diffuse

        #ifdef NORMALMAP_SLOPE
            vec4 slope_x = texture2D(m_NormalMap_slope, vec2(coords.x * m_DiffuseMap_slope_scale, coords.y*m_DiffuseMap_slope_vscale));
            vec4 slope_z = texture2D(m_NormalMap_slope, vec2(coords.z * m_DiffuseMap_slope_scale, coords.y*m_DiffuseMap_slope_vscale));

            normal = blending.y * normal + blending.x * slope_z + blending.z * slope_x;
        #else
            normal = blending.y * normal + blending.x * vec3(0.5,0.5,1) + blending.z * vec3(0.5,0.5,1);
        #endif
    #endif

    normal = (normal.xyz * vec3(2.0) - vec3(1.0));
    return normalize(normal);
}

void main(){

    //----------------------
    // diffuse calculations
    //----------------------
    #ifdef DIFFUSEMAP
        vec4 diffuseColor = calculateTriPlanarDiffuseBlend(wNormal, wVertex);
    #else
        vec4 diffuseColor = vec4(1.0);
    #endif

        float spotFallOff = 1.0;
        if(g_LightDirection.w!=0.0){
              vec3 L=normalize(lightVec.xyz);
              vec3 spotdir = normalize(g_LightDirection.xyz);
              float curAngleCos = dot(-L, spotdir);             
              float innerAngleCos = floor(g_LightDirection.w) * 0.001;
              float outerAngleCos = fract(g_LightDirection.w);
              float innerMinusOuter = innerAngleCos - outerAngleCos;

              spotFallOff = (curAngleCos - outerAngleCos) / innerMinusOuter;

              if(spotFallOff <= 0.0){
                  gl_FragColor = AmbientSum * diffuseColor;
                  return;
              }else{
                  spotFallOff = clamp(spotFallOff, 0.0, 1.0);
              }
        }

    //---------------------
    // normal calculations
    //---------------------
    #if defined(NORMALMAP) || defined(NORMALMAP_1) || defined(NORMALMAP_2) || defined(NORMALMAP_3)
        vec3 normal = calculateNormalTriPlanar(wNormal, wVertex);
    #else
        vec3 normal = vNormal;
    #endif


    //-----------------------
    // lighting calculations
    //-----------------------
    vec4 lightDir = vLightDir;
    lightDir.xyz = normalize(lightDir.xyz);

    vec2 light = computeLighting(normal, vViewDir.xyz, lightDir.xyz,lightDir.w*spotFallOff,Shininess);

    vec4 specularColor = vec4(1.0);

    //--------------------------
    // final color calculations
    //--------------------------
    gl_FragColor =  AmbientSum * diffuseColor +
                    DiffuseSum * diffuseColor  * light.x +
                    SpecularSum * specularColor * light.y;

    //gl_FragColor.a = alpha;
}
