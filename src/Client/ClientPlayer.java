/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import com.jme3.collision.CollisionResults;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.math.FastMath;
import com.jme3.math.Matrix3f;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import com.jme3.terrain.geomipmap.TerrainQuad;
import java.io.IOException;

/**
 *
 * @author Win
 */
public class ClientPlayer implements Control {
    Spatial model;
    int playerId;
    String playerName;
    
    //Position
    private Vector2f posOld;
    private Vector2f posNew;
    private Vector2f posNow;
    
    //Rotation
    private float rotOld;
    private float rotNew;
    private float rotNow;
    
    private float timeout;
    private float totalTime = 0;
    private boolean interpolateBegin = false;
    private float progress = 0;
    
    private float height;
    
    private TerrainQuad terrain;
    
    public ClientPlayer(Spatial playerModel, int id, String name, float height, TerrainQuad terrain) {
        this.model = playerModel;
        this.playerId = id;
        this.playerName = name;
        this.height = height;
        this.terrain = terrain;
        
        this.posOld = Vector2f.ZERO;
        this.posNow = Vector2f.ZERO;
        
        this.rotOld = 0f;
        this.rotNow = 0f;
    }

    @Override
    public Control cloneForSpatial(Spatial spatial) {
        ClientPlayer newClient = new ClientPlayer(model, playerId, playerName, height, terrain);
        spatial.addControl(newClient);
        return newClient;
    }

    @Override
    public void setSpatial(Spatial spatial) {
        this.model = spatial;
    }
    
    public String getName() {
        return this.playerName;
    }
    
    public int getId() {
        return playerId;
    }
    
    public float getHeight(Node check) {

        CollisionResults result = new CollisionResults();
        CollisionResults resultUp = new CollisionResults();
        
        Ray rayDown = new Ray(model.getWorldTranslation(), new Vector3f(0, -1, 0));
        Ray rayUp = new Ray(model.getWorldTranslation(), new Vector3f(0, 1, 0));
        
        check.collideWith(rayDown, result);
        check.collideWith(rayUp, resultUp);

        if (result.size() > 0) {
            return model.getLocalTranslation().getY() - result.getClosestCollision().getDistance() + this.height;
        } else if (resultUp.size() > 0) {
            System.out.println("[err] Character below world!");
            return model.getLocalTranslation().getY() + resultUp.getClosestCollision().getDistance() + this.height;
        }
        
        return 0;
    }
    
    //Specify a new position for the player and the time to move there
    public void setUpdatedPosition(Vector2f pos, float rot, int timeout) {
        this.timeout = timeout/1000f; //Convert to seconds
        
        this.posNew = pos;
        this.posOld = posNow;
        this.totalTime = 0;
        
        this.rotNew = rot;
        this.rotOld = rotNow;
        
        interpolateBegin = true;
    }

    @Override
    public void update(float tpf) {
        //Where the fun begins, updating the spatial position
        //First we set a linear interpolation for the current position to the final position
        if (interpolateBegin) {
            
            totalTime += tpf;
            progress = totalTime/timeout;
            
            posNow = posOld.interpolateLocal(posNew, progress);
            
            rotNow = FastMath.interpolateLinear(progress, rotOld, rotNew); //Rudimetary rotary interpolation
            //rotNow = (rotNew-rotOld)*progress + rotOld; //Rudimetary rotary interpolation
            
            //Set the posNow location
            model.setLocalTranslation(new Vector3f(posNow.getX(), getHeight(terrain), posNow.getY()));
            
            //Construct a matrix about the Y axis for rotation
            Matrix3f finalRotation = new Matrix3f();
            finalRotation.fromAngleAxis(rotNow, Vector3f.UNIT_Y);
            
            model.setLocalRotation(finalRotation);
            
            if (progress > 1) {
                //Finished interpolation
                posOld = posNew;
                rotOld = rotNew;
                
                interpolateBegin = false;
                totalTime = 0;
            }
        }
    }

    @Override
    public void render(RenderManager rm, ViewPort vp) {
        //No rendering functions yet required.
    }

    @Override
    public void write(JmeExporter ex) throws IOException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void read(JmeImporter im) throws IOException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
