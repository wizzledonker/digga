/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import com.jme3.terrain.geomipmap.TerrainQuad;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import mygame.Digga;

/**
 *
 * @author Win
 */
public class ClientTerrainModifier implements Control {
    TerrainQuad terrain;
    Digga mainGame;
    
    //Variables that control the digging that actually takes place.
    Map<Vector2f, Float> digs = new ConcurrentHashMap();
    Map<Vector2f, Float> deposits = new ConcurrentHashMap();
    
    //Set of particle emitters that are to be removed when they're done
    ParticleEmitter debris;
    
    float stepSize;
    
    //Global variables that can be changed per instance
    float digSpeed = 12.0f;
    
    public ClientTerrainModifier(TerrainQuad terrain, float step, Digga instance) {
        this.terrain = terrain;
        this.stepSize = step;
        this.mainGame = instance;
        
        //Make the dirt particles
        debris =
                new ParticleEmitter("Debris", ParticleMesh.Type.Triangle, 1024);
        Material debris_mat = new Material(mainGame.getAssetManager(),
                "Common/MatDefs/Misc/Particle.j3md");
        debris_mat.setTexture("Texture", mainGame.getAssetManager().loadTexture(
                "Textures/Particles/grassAlpha.png"));
        
        debris.setMaterial(debris_mat);
        
        debris.setImagesX(4);
        debris.setImagesY(1); // 4x3 texture animation
        //debris.setRotateSpeed(4);
        debris.setSelectRandomImage(true);
        debris.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 6, 0));
        
        //Start and end size
        debris.setStartColor(ColorRGBA.Brown);
        debris.setEndSize(0.2f);
        debris.setStartSize(0.2f);
        
        debris.setGravity(0, 10, 0);
        debris.getParticleInfluencer().setVelocityVariation(.80f);
        debris.setLowLife(2.0f);
        debris.setHighLife(2.0f);
        debris.setParticlesPerSec(0);
        
        mainGame.getRootNode().attachChild(debris);
    }

    @Override
    public Control cloneForSpatial(Spatial spatial) {
        ClientTerrainModifier terrMod = new ClientTerrainModifier((TerrainQuad) spatial, stepSize, mainGame);
        return terrMod;
    }

    @Override
    public void setSpatial(Spatial spatial) {
        this.terrain = (TerrainQuad) spatial;
    }
    
    public void digAtLocation(Vector2f location) {
       // if (!digs.isEmpty()) return;
        throwParticles(new Vector3f(location.getX(), terrain.getHeight(location), location.getY()));
        digs.put(location, 0f);
    }
    
    public void depositAtLocation(Vector2f location) {
        deposits.put(location, 0f);
    }
    
    public void throwParticles(Vector3f loc) {
        //Update the location and throw the particles again
        debris.setLocalTranslation(loc);
        debris.emitParticles(64);
    }

    @Override
    public void update(float tpf) {
        if (!digs.isEmpty()) {
            for (Vector2f loc : digs.keySet()) {
                //For every dig, update the progress and change the elevation at the point
                terrain.adjustHeight(loc, -tpf*digSpeed);
                digs.put(loc, digs.get(loc)+tpf*digSpeed);
                
                if (digs.get(loc) > stepSize) {
                    digs.remove(loc);
                    //Create a particle effect at the location to indicate that digging is done
                }
            }
        }
        
        if (!deposits.isEmpty()) {
            for (Vector2f loc : deposits.keySet()) {
                //For every dig, update the progress and change the elevation at the point
                terrain.adjustHeight(loc, tpf*digSpeed);
                deposits.put(loc, deposits.get(loc)+tpf*digSpeed);
                
                if (deposits.get(loc) > stepSize) {
                    deposits.remove(loc);
                }
            }
        }
    }

    @Override
    public void render(RenderManager rm, ViewPort vp) {
        //To be implemented
    }

    @Override
    public void write(JmeExporter ex) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void read(JmeImporter im) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
