/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Multiplayer;

import com.jme3.network.Client;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import mygame.Digga;
import mygame.Messages.ChatMessage;

/**
 *
 * @author Win
 */
public class MessageHandler implements MessageListener<Client> {
    private final Digga mainGame;
    
    public MessageHandler(Digga instance) {
        mainGame = instance;
    }
    
    @Override
    public void messageReceived(Client source, Message m) {
        if (m instanceof ChatMessage) {
            ChatMessage msg = (ChatMessage) m;
            
            mainGame.receiveChatMessage(msg.getName(), msg.getMessage());
        }
    }
    
}
