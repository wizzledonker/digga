/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Multiplayer;

import Entities.Player;
import Events.TerrainModifyEvent;
import com.jme3.network.Client;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.scene.Spatial;
import java.util.concurrent.Callable;
import mygame.Digga;
import mygame.Messages.PlayerConnectMessage;
import mygame.Messages.PlayerDisconnectMessage;
import mygame.Messages.PlayerSnapshot;
import mygame.Messages.WorldSnapshot;

/**
 *
 * @author Win
 */
public class PlayerHandler implements MessageListener<Client> {
    private final Digga mainGame;
    
    public PlayerHandler(Digga instance) {
        mainGame = instance;
    }
    @Override
    public void messageReceived(Client source, Message m) {
        
        //Check if we've properly initialized our client
        if(!mainGame.isConnected) {
            return;
        }
        
        if (m instanceof PlayerConnectMessage) {
            PlayerConnectMessage cMsg = (PlayerConnectMessage) m;
            
            //Check if the player is us, and if not add it to the list of player entities. If it is, we update the local time and set the playerid
            if (cMsg.getPlayerName().equals(mainGame.getPlayerName())) {
                //The player is us
                onSuccessfulConnect(source.getId());
            } else {
                mainGame.addPlayer(cMsg.getPlayerName(), cMsg.getPlayerId());
                System.out.println("Player added to world: " + cMsg.getPlayerName() + " with id = " + cMsg.getPlayerId());
            }
            
            mainGame.addChat(cMsg.getPlayerName() + " has joined the game.");
        } else if (m instanceof PlayerDisconnectMessage) {
            
            PlayerDisconnectMessage cMsg = (PlayerDisconnectMessage) m;
            String playerName = mainGame.getPlayerNameById(cMsg.getId());
            
            System.out.println("Player removed from world: " + playerName);
            mainGame.addChat(playerName + " disconnected.");
            mainGame.removePlayer(playerName);
            
        } else if (m instanceof WorldSnapshot) {
            WorldSnapshot ws = (WorldSnapshot) m;
            
            //Ignore this packet if it's from the past
            if (ws.getServerTime() < mainGame.clientTime) {
                return;
            }
            
            //Send our player's position to the server
            mainGame.sendPlayerPosition();
            
            for (Player player : ws.getPlayers()) {
                
                //Don't worry if the player is us
                if (player.getClientId() == mainGame.getPlayerId()) {
                    continue;
                }
                
                //Update the player name
                player.setName(mainGame.getPlayerNameById(player.getClientId()));
                
                //Check if we haven't got that player in our database, and if so we add them (works well for adding players who started offline ;)
                if (mainGame.getPlayerById(player.getClientId()) == null && mainGame.worldDownloaded) {
                    //Add the player
                    mainGame.addPlayer(player.getName(), player.getClientId());
                }

                mainGame.updatePlayer(player.getName(), player.getPosition(), player.getRotation(), ws.getTicktime());
            }
            
            if (ws.getEvent() != null) {
                if (ws.getEvent() instanceof TerrainModifyEvent) {
                    final TerrainModifyEvent evt = (TerrainModifyEvent) ws.getEvent();
                    
                    mainGame.enqueue(new Callable<Spatial>() {
                        @Override
                        public Spatial call() throws Exception {
                            if (evt.isDigging()) {
                                mainGame.getTerrainModifier().digAtLocation(evt.getPosition());
                            } else {
                                mainGame.getTerrainModifier().depositAtLocation(evt.getPosition());
                            }
                            return mainGame.getRootNode();
                        }
                    });
                }
            }
            
            mainGame.setTimeOfDay(ws.getServerTime());
            
        }
    }
    
    private void onSuccessfulConnect(int id) {
        mainGame.closePopup();
            
        mainGame.setPaused(false);
        
        System.out.println("Player id = " + id);
        mainGame.setPlayerId(id);
    }
    
}
