
package Multiplayer;

import com.jme3.network.Client;
import com.jme3.network.ClientStateListener;
import mygame.Digga;

/**
 *
 * @author Win
 */
public class ConnectionHandler implements ClientStateListener {
    private Digga mainApp;
    
    public ConnectionHandler(Digga instance) {
        mainApp = instance;
    }

    @Override
    public void clientConnected(Client c) {
        mainApp.isConnected = true;
        mainApp.setPlayerId(c.getId());
    }

    @Override
    public void clientDisconnected(Client c, DisconnectInfo info) {
        
        if (info == null) return;
        
        mainApp.isConnected = false;
        //Add message in response to being disconnected
        mainApp.kickedServerReason(info.reason);
        //mainApp.addChat("Disconnected. Reason: " + info.reason);
    }
}
