/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Multiplayer;

import com.jme3.network.Client;
import com.jme3.network.ErrorListener;
import com.jme3.network.serializing.SerializerException;

/**
 *
 * @author Win
 */
public class ErrorHandler implements ErrorListener<Client> {

    @Override
    public void handleError(Client source, Throwable t) {
        //For now, we're not handling any errors with the connection. Just treating the error as a reportable
        if (t.getCause() instanceof SerializerException) {
            System.out.println("Error with deserialization before initialisation... Retrying connection...");
        } else {
            System.out.println("Terminating connection with error: " + t.getLocalizedMessage());
            source.close();
        }
    }
    
}
