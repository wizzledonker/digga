/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Multiplayer;

import com.jme3.network.Client;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import mygame.Digga;
import mygame.Messages.HandshakeMessage;
import mygame.Messages.TerrainMessage;

/**
 *
 * @author Win
 */
public class TerrainHandler implements MessageListener<Client> {
    private final Digga mainGame;
    
    private float[] builtTerrain;
    
    public TerrainHandler(Digga instance) {
        mainGame = instance;
    }

    @Override
    public void messageReceived(Client source, Message m) {
        if (m instanceof TerrainMessage) {
            //We're downloading terrain now
            TerrainMessage msg = (TerrainMessage) m;
            
            //First packet operations
            if (msg.getCurrentChunk() == 0) {
                builtTerrain = new float[msg.getHeightMapSize()*msg.getHeightMapSize()];
            }
            
            for (int i = 0; i < (128*128); i++) {
                builtTerrain[i + (128*128)*msg.getCurrentChunk()] = (float) msg.getHeightMap()[i];
            }
            
            System.out.println("Terrain message recieved... Chunk ID = " + msg.getCurrentChunk());
            
            if (msg.getCurrentChunk() == msg.getTotalChunks()-1) {
                //Go ahead and create the terrain, then unpause the game!
                mainGame.createTerrain(msg.getHeightMapSize(), builtTerrain, msg.getStepSize());

                System.out.println("Sending Handshake...");
                source.send(new HandshakeMessage(mainGame.getPlayerName()));

            }
        }
    }
    
}
