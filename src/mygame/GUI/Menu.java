
package mygame.GUI;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.BaseAppState;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.SliderChangedEvent;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import mygame.Digga;

/**
 *  
 *  
 *
 * @author Win 
 */
public class Menu extends BaseAppState implements ScreenController {
    public Nifty mainUI;
    public Screen current;
    
    Element quitElement;
    Element loginElement;
    Element optionsElement;
    
    boolean fullScreen;
    
    Digga mainApp;
    
    public Menu(boolean fs, SimpleApplication app) {
        fullScreen = fs;
        mainApp = (Digga) app;
    }
    
    @Override
    protected void initialize(Application app) { 
        
        
    }
    
    @Override
    protected void cleanup(Application app) {
        //TODO: clean up what you initialized in the initialize method,        
        //e.g. remove all spatials from rootNode
    }

    
    @Override
    protected void onEnable() {
        
    }
    
    @Override
    protected void onDisable() {
        
    }

    @Override
    public void update(float tpf) {
        //TODO: implement behavior during runtime    
    }

    @Override
    public void bind(Nifty nifty, Screen screen) {
        //Bind the screen to make it work
        mainUI = nifty;
        current = screen;
        
        quitElement = mainUI.createPopup("popupExit");
        loginElement = mainUI.createPopup("popupJoin");
        optionsElement = mainUI.createPopup("popupOptions");
    }

    @Override
    public void onStartScreen() {
        
        if (mainUI.getTopMostPopup() != null) {
            cancel();
        }
        
        //When the screen starts, it's needed to update its state
        if (mainApp.joinBoxUp) {
            mainUI.showPopup(current, loginElement.getId(), null);
        }
        
        loginElement.findElementById("helperText").getRenderer(TextRenderer.class).setText(mainApp.disconnectReasonText);
    }
    
    //Handler for FOV changes
    @NiftyEventSubscriber(id="fov")
    public void onFOVChanged(String id, SliderChangedEvent event) {
        //TerraNullius.config.changeFOV(event.getValue());
        mainApp.getCamera().setFrustumPerspective(event.getValue(), mainApp.getCamera().getWidth()/mainApp.getCamera().getHeight(), 0.1f, 1000f);
    }
    
    //Handler for mouse sensitivity changes
    @NiftyEventSubscriber(id="mss")
    public void onMouseChanged(String id, SliderChangedEvent event) {
        //TerraNullius.config.changeMouseSensitivity(event.getValue());
        mainApp.getFPSControl().setSensitivity(event.getValue());
    }
    
        //Three different popups that we can access from the menu
    public void options() {
        mainUI.showPopup(current, optionsElement.getId(), null);
    }
    
    public void quit() {
        mainUI.showPopup(current, quitElement.getId(), null);
    }
    
    public void login() {
        mainUI.showPopup(current, loginElement.getId(), null);
    }
    
    public void start() {
        //This is called when the start button is clicked
        mainApp.setPaused(false);
    }
    
    public void processJoin() {
        //Process the join
        TextField name = current.findNiftyControl("name", TextField.class);
        TextField ipNum = current.findNiftyControl("ip", TextField.class);
        
        String ip = ipNum.getRealText();
        mainApp.setPlayerName(name.getRealText()); //Update the player's name when we go to join a game
        
        Element update = current.findElementById("helperText");
        mainApp.joinServer(ip, update);
        
        
        //current.findElementById("loginLabel").getRenderer(TextRenderer.class).setText("Logged in as: " + username.getRealText());
    }
    
    public void realQuit() {
        //Quitting
        mainApp.leaveServer();
        
        //Quit
        mainApp.stop();
    }
    
    public void cancel() {
        mainUI.closePopup(current.getTopMostPopup().getId());
    }

    @Override
    public void onEndScreen() {
        //Nothing yet...
    }
}
