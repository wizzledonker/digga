package mygame;

import Client.ClientPlayer;
import Client.ClientTerrainModifier;
import Multiplayer.ConnectionHandler;
import Multiplayer.MessageHandler;
import Multiplayer.PlayerHandler;
import Multiplayer.TerrainHandler;
import com.jme3.app.SimpleApplication;
import com.jme3.bounding.BoundingSphere;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.network.Client;
import com.jme3.network.Network;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.BloomFilter;
import com.jme3.post.filters.LightScatteringFilter;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;
import com.jme3.shadow.DirectionalLightShadowFilter;
import com.jme3.terrain.geomipmap.TerrainLodControl;
import com.jme3.terrain.geomipmap.TerrainQuad;
import com.jme3.terrain.geomipmap.lodcalc.DistanceLodCalculator;
import com.jme3.terrain.heightmap.AbstractHeightMap;
import com.jme3.terrain.heightmap.HillHeightMap;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture.WrapMode;
import com.jme3.texture.Texture2D;
import com.jme3.water.WaterFilter;
import de.lessvoid.nifty.EndNotify;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ElementBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.TextBuilder;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import mygame.GUI.Menu;
import mygame.Messages.ChatMessage;
import mygame.Messages.PlayerConnectMessage;
import mygame.Messages.PlayerDisconnectMessage;
import mygame.Messages.PlayerSnapshot;
import mygame.Messages.TerrainMessage;
import mygame.Messages.TerrainModifyMessage;
import mygame.Messages.WorldSnapshot;


/**
 * This is the Main Class of your Game. You should only do initialization here.
 * Move your Logic into AppStates or Controls
 * @author normenhansen
 */
public class Digga extends SimpleApplication implements ActionListener {
    
    public static final String NAME = "Digga";
    public static final int VERSION = 1;
    public static final int PORT = 5110;
    public static final int UDP_PORT = 5110;
    
    private Client client;
    private int chatID = 0;
    
    private TerrainQuad terrain;
    Material terrainMaterial;
    FirstPersonCamera fpsControl;
    
    private final float textureScale = 2f;
    private final float cliffTextureScale = 0.1f;
    private final float cliffVerticalScale = 0.025f;
    
    //Day/Night cycle stuff
    DirectionalLight sun;
    AmbientLight ambient;
    
    private Vector3f sunDirection = new Vector3f(-0.2f, -1.0f, 0.0f);
    float skyDomeRadius = 800;
    float initialWaterHeight = 16f;
    
    int dayLength = 10*60*1000; //10 Minute days
    float currentRot = FastMath.PI; //Rotation. Midday = PI
    
    //Primitives that are updated when the sun direction is
    WaterFilter water;
    LightScatteringFilter waterScatter;
    Geometry skyGeometry;
    
    //Shadow and shader settings
    final int SHADOWMAP_SIZE = 1024;
    
    //-- NIFTY GUI overlay --//
    Nifty nifty;
    private boolean logicPaused;
    private boolean chatUp;
    private LinkedList<String> chatHistory;
    private int messageDisplay = 5; //History of messages to keep
    private boolean gameInitialized = false;
    
    //-- MULTIPLAYER VARIABLES --//
    private Node playerNode;
    private String playerName;
    private int playerID;
    public int clientTime;
    public boolean isConnected = false;
    public boolean worldDownloaded = false;
    
    public String disconnectReasonText = "Multiplayer is very buggy :)";
    public boolean joinBoxUp = false;
    
    // -- Globals -- //
    ColorRGBA horizonColor = new ColorRGBA(0.94f, 0.96f, 0.98f, 1.0f);
    ColorRGBA zenithColor = new ColorRGBA(0.31f, 0.48f, 1.00f, 1.0f);
    
    ColorRGBA d_horizonColor = new ColorRGBA(0.94f, 0.96f, 0.98f, 1.0f);
    ColorRGBA d_zenithColor = new ColorRGBA(0.31f, 0.48f, 1.00f, 1.0f);
    
    ColorRGBA s_horizonColor = new ColorRGBA(0.97f, 0.37f, 0.02f, 1.0f);
    ColorRGBA s_zenithColor = new ColorRGBA(0.40f, 0.31f, 1.00f, 0.5f);
    
    ColorRGBA n_horizonColor = new ColorRGBA(0.40f, 0.31f, 1.00f, 0.5f).mult(0.5f);
    ColorRGBA n_zenithColor = ColorRGBA.BlackNoAlpha;

    public static void main(String[] args) {
        Digga app = new Digga();
        //app.settings.setSettingsDialogImage("splash.png");
        app.start();
    }
    
    public FirstPersonCamera getFPSControl() {
        return fpsControl;
    }
    
    public void setPlayerName(String name) {
        this.playerName = name;
    }
    
    public String getPlayerName() {
        return playerName;
    }
    
    //Only performed once, this sets the ID of the player in the multiplayer server
    public void setPlayerId(int id) {
        this.playerID = id;
    }
    
    public int getPlayerId() {
        return playerID;
    }
    
    //Methods to add players, only called after the game has started
    //Note that this doesn't actually add the player to the scene, it just marks it to happen on the next 
    public void addPlayer(String name, int id) {
        
        //First create a cube
        Box b = new Box(0.5f, fpsControl.getPlayerHeight(), 0.5f);
        final Geometry playerGeom = new Geometry(name, b); //Create a new geometry with the player name and ID so we don't get confused
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.randomColor());
        playerGeom.setMaterial(mat);
        
        ClientPlayer playerControl = new ClientPlayer(playerGeom, id, name, fpsControl.getPlayerHeight(), this.terrain);
        playerGeom.addControl(playerControl);
        
        playerGeom.setName(name);
        
        this.enqueue(new Callable<Spatial>() {
            @Override
            public Spatial call() throws Exception {
                //Only modify the scene when we're well and ready
                playerNode.attachChild(playerGeom);
                return playerGeom;
            }
        });
    }
    
    /**
     * Gets a player that's connect by ID
     * @param id The player ID to search for
     * @return ClientPlayer representing this player.
     */
    public ClientPlayer getPlayerById(int id) {
        for (Spatial player : playerNode.getChildren()) {
            ClientPlayer play = player.getControl(ClientPlayer.class);
            if (play.getId() == id) {
                return play;
            }
        }
        return null;
    }
    
    public ClientPlayer getPlayer(String name) {
        Spatial playerSpat = playerNode.getChild(name);
        
        if (playerSpat == null) {
            logError("A get player call for " + name + " was null.");
            return null;
        }
        
        return playerSpat.getControl(ClientPlayer.class);
    }
    
    public void sendTerrainMessage(float x, float y, boolean dig) {
        client.send(new TerrainModifyMessage(x, y, dig));
    }
    
    public void sendPlayerPosition() {
        this.sendPosition(fpsControl.getPosition(), fpsControl.getRotation());
    }
    
    private void sendPosition(Vector2f pos, float rot) {
        PlayerSnapshot msg = new PlayerSnapshot(this.getPlayerId(), pos.getX(), pos.getY(), rot);
        msg.setReliable(false);
        client.send(msg);
    }
    
    public void logError(String err) {
        System.err.println("[Logic ERROR] " + err);
    }
    
    public void setTimeOfDay(int millis) {
        this.clientTime = millis;
        
        Vector3f playerPos = new Vector3f(fpsControl.getPosition().getX(), 0.0f, fpsControl.getPosition().getY());
        
        //Debug code for rotating the sun's position in the sky
        currentRot = (FastMath.PI + (((float) millis/dayLength))*2f*FastMath.PI);
        sunDirection.setX(FastMath.sin(currentRot));
        sunDirection.setY(FastMath.cos(currentRot));
        
        sun.setDirection(sunDirection.normalize());
        water.setLightDirection(sunDirection);
        waterScatter.setLightPosition(sunDirection.mult(-skyDomeRadius).add(playerPos));
        
        //Update the colour of the sky depending on the time of day
        float sunsetFactor = ((float) millis/(dayLength/2f))%1f; //number between 0 and 1 representing the time of day
        sunsetFactor = FastMath.pow(FastMath.abs(sunsetFactor-0.5f)*2f, 0.5f); //The closer values get to the horizons, the closer to 1 they get
        //System.out.println(sunsetFactor);
        
        float nightFactor = ((float) millis/dayLength)%1f;
        nightFactor = Math.min(FastMath.pow(FastMath.abs(nightFactor-0.5f)*4f, 2), 1.0f);
        
        Material skyMat = skyGeometry.getMaterial();
        ColorRGBA h = n_horizonColor.clone().interpolateLocal(s_horizonColor.clone().interpolateLocal(horizonColor, sunsetFactor), nightFactor);
        ColorRGBA z = n_zenithColor.clone().interpolateLocal(s_zenithColor.clone().interpolateLocal(zenithColor, sunsetFactor), nightFactor);
        skyMat.setColor("horizonColor", h);
        skyMat.setColor("zenithColor", z);
    }
    
    public void updatePlayer(String name, Vector2f pos, float rot, int time) {
        ClientPlayer play = getPlayer(name);
        if (play != null) {
            play.setUpdatedPosition(pos, rot, time); //Set a 100ms timeout for position changes
        } else {
            logError("Tried to update a player " + name + ", but they were null!");
        }
    }
    
    public void removePlayer(String name) {
        final Spatial playerSpat = playerNode.getChild(name);
        
        this.enqueue(new Callable<Spatial>() {
            @Override
            public Spatial call() throws Exception {
                //Only modify the scene when we're well and ready
                if (playerSpat != null) playerSpat.removeFromParent();
                return playerSpat;
            }
        });
    }
    
    public ClientTerrainModifier getTerrainModifier() {
        return terrain.getControl(ClientTerrainModifier.class);
    }
    
    public String getPlayerNameById(int id) {
        ClientPlayer play = getPlayerById(id);
        return play == null ? "NullPlayer" : play.getName();
    }
  
    public void joinServer(String ip, Element callback) {
        //This function connects to a server with a specific IP, logging to the callback textfield
        callback.getRenderer(TextRenderer.class).setText("Connecting to '" + ip + "'...");
        
        //Reset flags
        worldDownloaded = false;
        isConnected = false;
        
        try {
            //Initialise the client
            client = Network.connectToServer(NAME, VERSION, ip, PORT, UDP_PORT);
            
        } catch (IOException ex) {
            Logger.getLogger(Digga.class.getName()).log(Level.WARNING, null, ex);
            callback.getRenderer(TextRenderer.class).setText("Failed connecting to '" + ip + "'. " + ex.getLocalizedMessage());
            return;
        }
        
        clientTime = 0;
        
        //The classes are automatically registered, so now we just connect and go ahead and download the server's world
        //Set a callback for when the world download message has been successfully sent
        client.addMessageListener(new TerrainHandler(this), TerrainMessage.class);
        client.addMessageListener(new MessageHandler(this), ChatMessage.class);
        client.addMessageListener(new PlayerHandler(this), PlayerConnectMessage.class, PlayerDisconnectMessage.class, WorldSnapshot.class);
        client.addClientStateListener(new ConnectionHandler(this));
        //client.addErrorListener(new ErrorHandler());
        
        client.start();
        
        
        callback.getRenderer(TextRenderer.class).setText("Connected. Downloading world...");
    }
    
    public void kickedServerReason(String reason) {
        this.setPaused(true);
        
        //Show the popup if it isn't up
        this.disconnectReasonText = "Disconnected. Reason: " + reason;
        this.joinBoxUp = true;
    }
    
    public void leaveServer() {
        if (client != null) {
            if (client.isConnected()) {
                client.close();
            }
        }
    }
    
    public void closePopup() {
        this.joinBoxUp = false;
    }
    
    //These are the main methods responsible for the pausing of the game
    public void setPaused(boolean paused) {
        this.logicPaused = paused;
        
        //Change the fpscontrol status
        fpsControl.setEnabled(!paused);
        
        //Show a certain UI
        if (logicPaused == false) {
            nifty.gotoScreen("blank");
        } else {
            nifty.gotoScreen("Main");
        }
        //nifty.update();
    }
    
    public boolean getPaused() {
        return this.logicPaused;
    }
    
    //Methods for getting and setting chat
    public void setChat(boolean chat) {
        if (!logicPaused) {
            fpsControl.setEnabled(!chat);

            this.chatUp = chat;
            if (chatUp) {
                //Set the focus and make the chat bar visible
                nifty.getCurrentScreen().findElementById("chatLayer").setVisible(true);
                nifty.getCurrentScreen().findElementById("msg").setFocus();
            } else {
                TextField text = nifty.getCurrentScreen().findNiftyControl("msg", TextField.class);
                String textString = text.getRealText();
                if (!textString.isEmpty()) {
                    sendChatMessage(textString);
                    text.setText("");
                }
                nifty.getCurrentScreen().findElementById("chatLayer").setVisible(false);
            }
            nifty.update();
        }
    }
    
    public void receiveChatMessage(String name, String message) {
        addChat(name + ": " + message);
    }
    
    public void addChat(String text) {
        chatHistory.add(text);
        if (chatHistory.size() > messageDisplay) {
            chatHistory.pop(); //Don't let the chat get too big
        }
        updateChatUI();
    }
    
    private void updateChatUI() {
        if (!"blank".equals(nifty.getCurrentScreen().getScreenId()) || nifty.getCurrentScreen() == null) {
            return;
        }
        
        //Updates the text panel to include the message
        
        //Remove all the previous entries in one sweep, then when they're done, start adding the entries back in
        Element panel = nifty.getCurrentScreen().findElementById("innerTextPanel");
        panel.markForRemoval(new EndNotify() {
            @Override
            public void perform() {
                PanelBuilder panelBuild = new PanelBuilder("innerTextPanel");
                panelBuild.childLayout(ElementBuilder.ChildLayoutType.Vertical);
                
                //When the removal is finished, perform the adding
                chatID = 0;
                for (String chat : chatHistory) {
                    TextBuilder textBuild = new TextBuilder("chatLog" + chatID++);
                    textBuild.font("aurulent-sans-16.fnt");
                    textBuild.color("#E1FFFA");

                    textBuild.text(chat);

                    textBuild.textVAlign(ElementBuilder.VAlign.Bottom);
                    
                    panelBuild.text(textBuild);
                    
                }
                panelBuild.build(nifty, nifty.getCurrentScreen(), nifty.getCurrentScreen().findElementById("textPanel"));
                
                //Update the layout
                nifty.getCurrentScreen().layoutLayers();
                nifty.update();
            }
        });

    }
    
    public void sendChatMessage(String message) {
        client.send(new ChatMessage(this.playerName, message));
    }
    
    public boolean getChat() {
        return chatUp;
    }
    
    public void setupCamera() {
        this.setPauseOnLostFocus(false);
        flyCam.setEnabled(false);
        //this.flyCam.setMoveSpeed(64.0f);
        this.getViewPort().setBackgroundColor(ColorRGBA.Black);
       
        cam.setFrustumPerspective(90, getCamera().getWidth()/getCamera().getHeight(), 0.1f, 1000f);
        fpsControl = new FirstPersonCamera(cam, terrain, this);
        fpsControl.registerWithInput(inputManager, rootNode);
        cam.update();
       
        fpsControl.warp(new Vector3f(0, 128, 0));
    }
    
    public Nifty getNifty() {
        return nifty;
    }
    
    public void createUI() {
        chatHistory = new LinkedList();
        
        NiftyJmeDisplay niftyDisplay = NiftyJmeDisplay.newNiftyJmeDisplay(assetManager, inputManager, audioRenderer, viewPort);
        nifty = niftyDisplay.getNifty();
        
        nifty.fromXml("Interface/Main.xml", "Main", new Menu(this.settings.isFullscreen(), this));
        guiViewPort.addProcessor(niftyDisplay);
        
        //Disable our two nice cameras
        fpsControl.setEnabled(false);
        this.flyCam.setEnabled(false);
        
        //Go ahead and remove the escape exit mapping and replace it with a pause functionality
        getInputManager().deleteMapping(SimpleApplication.INPUT_MAPPING_EXIT);
        getInputManager().addMapping("Pause", new KeyTrigger(KeyInput.KEY_ESCAPE));
        getInputManager().addMapping("Chat", new KeyTrigger(KeyInput.KEY_RETURN));
        
        getInputManager().addListener(this, "Pause", "Chat");
    }
    
    //It's called createTerrain but really it's for re-instancing the whole world
    public void createTerrain(final int size, final float[] heightMap, final float stepSize) {

        //Update the scenegraph, do it only once the main thread is ready
        this.enqueue(new Callable<Spatial>() {
            @Override
            public Spatial call() throws Exception {
                
                //Remove the last terrain from existence
                if (rootNode.getChild("Terrain") != null) {
                    rootNode.getChild("Terrain").removeFromParent();
                }
                
                //Sets the terrain up using a terrain material
                terrainMaterial = new Material(assetManager, "MatDefs/TerrainLightingSlope.j3md");
                //terrainMaterial.getAdditionalRenderState().setWireframe(true);

                //TODO: Set an alpha mask for where to paint cliffs/etc

                //Firstly, set up the grass texture
                Texture grass = assetManager.loadTexture("Textures/Terrain/grass.png");
                grass.setWrap(WrapMode.Repeat);

                Texture cliffs = assetManager.loadTexture("Textures/Terrain/cliff.png");
                cliffs.setWrap(WrapMode.Repeat);

                Texture sand = assetManager.loadTexture("Textures/Terrain/sand_d.png");
                sand.setWrap(WrapMode.Repeat);

                Texture sandBeach = assetManager.loadTexture("Textures/Terrain/sand_1.png");
                sandBeach.setWrap(WrapMode.Repeat);

                terrainMaterial.setFloat("BlendRange", 16f);

                //Slope settings
                terrainMaterial.setFloat("DiffuseMap_slope_scale", cliffTextureScale);
                terrainMaterial.setFloat("DiffuseMap_slope_vscale", cliffVerticalScale);
                terrainMaterial.setTexture("DiffuseMap_slope", cliffs);

                //Grass Settings
                terrainMaterial.setFloat("DiffuseMap_0_scale", textureScale);
                terrainMaterial.setTexture("DiffuseMap", grass);

                //Below water Settings
                terrainMaterial.setFloat("DiffuseMap_2_min", -1.0f);
                terrainMaterial.setFloat("DiffuseMap_2_max", 8.0f);
                terrainMaterial.setFloat("DiffuseMap_2_scale", textureScale);
                terrainMaterial.setTexture("DiffuseMap_2", sand);
                
                //Beach settings
                terrainMaterial.setFloat("DiffuseMap_3_min", 8.0f);
                terrainMaterial.setFloat("DiffuseMap_3_max", 16.0f);
                terrainMaterial.setFloat("DiffuseMap_3_scale", textureScale);
                terrainMaterial.setTexture("DiffuseMap_3", sandBeach);

                //terrainMaterial.setTexture("NormalMap_2", sand_n);

                //Infinite generation (can switch in no time!)
                
                /*FractalSum base = new FractalSum();
                base.setRoughness(0.8f);
                base.setFrequency(1.0f);
                base.setAmplitude(1.0f);
                base.setLacunarity(1.83f);
                base.setOctaves(8);
                base.setScale(0.03f);
                base.addModulator(new NoiseModulator() {

                    @Override
                    public float value(float... in) {
                        return ShaderUtils.clamp(in[0] * 0.5f + 0.5f, 0, 1);
                    }
                });
                
                OptimizedErode therm = new OptimizedErode();
                therm.setRadius(5);
                therm.setTalus(0.011f);
                
                FilteredBasis noise = new FilteredBasis(base);
                noise.addPreFilter(therm);
                terrain = new TerrainGrid("Terrain", 65, 129, new FractalTileLoader(base, 64.0f));
                TerrainGridLodControl control = new TerrainGridLodControl(terrain, getCamera()); */
                
                //Static generation
                terrain = new TerrainQuad("Terrain", 33, size+1, heightMap);
                TerrainLodControl control = new TerrainLodControl(terrain, getCamera());
                ClientTerrainModifier terrainModifyControl = new ClientTerrainModifier(terrain, stepSize, Digga.this);

                control.setLodCalculator(new DistanceLodCalculator(33, 2f)); //Later this will be configurable in graphics settings

                //Set terrain properties
                terrain.addControl(control);
                terrain.addControl(terrainModifyControl);
                terrain.setMaterial(terrainMaterial);

                //terrain.setLocalTranslation(0, -100, 0); //Move the terrain over somewhat. Believe me, later this'll be calculated :)
                terrain.setLocalScale(2.0f, 1f, 2.0f); //Just in case we need to change dimensions, leaving this in here.
                terrain.recalculateAllNormals();
                
                //terrain.setShadowMode(RenderQueue.ShadowMode.Receive);
                
                rootNode.attachChild(terrain);
                
                //Setup the first person camera for the new terrain
                setupCamera();
                
                //Things that only happen the first time the terrain is generated
                if (!gameInitialized) {

                    postInitialization();
                    
                    gameInitialized = true;
                } else  {
                    worldDownloaded = true;
                }
                
                return terrain;
            }
        });
        
    }
    
    /**
     * Called afer the game has been properly initialized with the terrain. Safe function for game-necessary items
     */
    private void postInitialization() {
        
        //Create the lights
        createLighting();

        //Create the cool post-processing effects
        createPostProcess();

        //User interface for networking
        createUI();

        setTimeOfDay(80*1000);
    }
    
    private void createLighting() {
        //The lighting scheme is going to be a simple directional light for now
        sun = new DirectionalLight();
        sun.setDirection(this.sunDirection.normalize()); //Basic definition for middle 45 degree light direction
        rootNode.addLight(sun);
        
        ambient = new AmbientLight();
        ambient.setColor(horizonColor.clone().interpolateLocal(zenithColor, 0.5f).mult(0.5f)); //Halfway between the two
        rootNode.addLight(ambient);
    }
    
    private void createPostProcess() {
        
        //Create a big box that represents the sky, add the sky material and flip the normals so they face inwards
        Material skyMat = new Material(assetManager, "MatDefs/SkyMaterial.j3md");
        skyMat.getAdditionalRenderState().setFaceCullMode(RenderState.FaceCullMode.Front); //Set face cull off for now (TODO: Switch to front)
        skyMat.setColor("horizonColor", horizonColor);
        skyMat.setColor("zenithColor", zenithColor);
        
        skyMat.setFloat("domeRadius", this.skyDomeRadius);
        skyMat.setFloat("sunScale", 64);
        
        Sphere skyBox = new Sphere(32, 32, skyDomeRadius); //Big dome
        skyGeometry = new Geometry("Sky", skyBox);
        
        //Set parameters for the sky
        skyGeometry.setQueueBucket(RenderQueue.Bucket.Sky);
        skyGeometry.setCullHint(Spatial.CullHint.Never);
        skyGeometry.setModelBound(new BoundingSphere(Float.POSITIVE_INFINITY, Vector3f.ZERO));
        skyGeometry.setMaterial(skyMat);
        
        rootNode.attachChild(skyGeometry);
        getCamera().setFrustumFar(2000f);
        
        water = new WaterFilter(rootNode, sunDirection);
        
        water.setWaterColor(new ColorRGBA().setAsSrgb(0.2f, 0.75f, 0.8f, 1.0f));
        water.setDeepWaterColor(new ColorRGBA().setAsSrgb(0.1f, 0.3f, 0.35f, 1.0f));
        water.setUnderWaterFogDistance(120);
        water.setWaterTransparency(0.12f);
        water.setFoamIntensity(0.3f);        
        water.setFoamHardness(0.3f);
        water.setFoamExistence(new Vector3f(0.8f, 8f, 1f));
        water.setReflectionDisplace(50);
        water.setRefractionConstant(0.25f);
        water.setColorExtinction(new Vector3f(30, 50, 70));
        water.setCausticsIntensity(0.4f);        
        water.setWaveScale(0.003f);
        water.setMaxAmplitude(1f);
        water.setFoamTexture((Texture2D) assetManager.loadTexture("Common/MatDefs/Water/Textures/foam2.jpg"));
        water.setRefractionStrength(0.2f);
        water.setWaterHeight(initialWaterHeight);
        
        //Bloom Filter
        BloomFilter bloom = new BloomFilter();        
        bloom.setExposurePower(55);
        bloom.setBloomIntensity(1.0f);
        
        //Light Scattering Filter
        waterScatter = new LightScatteringFilter(sunDirection.mult(-800f));
        waterScatter.setLightDensity(0.5f);
        
        //Shadow stuff
        DirectionalLightShadowFilter shadowFilter = new DirectionalLightShadowFilter(assetManager, SHADOWMAP_SIZE, 3);
        shadowFilter.setLight(sun);
        shadowFilter.setEnabled(true);
        
        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        
        fpp.addFilter(water);
        //fpp.addFilter(bloom);
        fpp.addFilter(waterScatter);
        fpp.addFilter(shadowFilter);
        
        viewPort.addProcessor(fpp);
    }

    @Override
    public void simpleInitApp() {
        
        setDisplayFps(false);
        setDisplayStatView(false);
        //Setup everything about the camera
        
        AbstractHeightMap heightMap = null;
        try {
            heightMap = new HillHeightMap(1024, 8192, 4, 64);
            heightMap.normalizeTerrain(128f);
        } catch (Exception ex) {
            System.out.println("Exception thrown when generating terrain!");
            ex.printStackTrace();
        }
        
        //Attach the player node (connecting and disconnecting players) to the root node
        playerNode = new Node("players");
        rootNode.attachChild(playerNode);
        
        createTerrain(heightMap.getSize(), heightMap.getScaledHeightMap(), 1.0f); // Initialise the terrain and put a random sample heightmap on it
        //createTerrainGrid() //To be implemented with infinite generation
    }

    @Override
    public void simpleUpdate(float tpf) {
        
        Vector3f playerPos = new Vector3f(fpsControl.getPosition().getX(), 0.0f, fpsControl.getPosition().getY());
        
        //Update the skybox to be on the player position
        this.skyGeometry.setLocalTranslation(playerPos);
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }

    @Override
    public void onAction(String name, boolean isPressed, float tpf) {
        if (name.equals("Pause") && !isPressed) {
            this.setPaused(!this.getPaused());
        }
        if (name.equals("Chat") && !isPressed) {
            if (this.isConnected) {
                this.setChat(!this.getChat());
            }
        }
    }
}
