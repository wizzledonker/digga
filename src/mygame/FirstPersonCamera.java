/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import Client.ClientTerrainModifier;
import com.jme3.collision.CollisionResults;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Node;
import com.jme3.scene.control.CameraControl;
import com.jme3.terrain.geomipmap.TerrainQuad;

/**
 *
 * @author Win
 */
public class FirstPersonCamera implements AnalogListener {

    protected boolean isEnabled;
    
    //Attributes required for the operation of the camera
    protected Camera cam;
    protected CameraNode sceneCam;
    protected TerrainQuad terrainToCheck;
    private InputManager input;
    private Digga mainGame;
    
    //Camera Attributes
    protected float walkSpeed = 6f;
    protected float runSpeed = 12f;
    protected float sensitivity = 12f;
    protected float playerHeight = 1.5f;
    
    private boolean running = false;
    
    protected float camPan = 0.0f;
    protected float camTilt = 0.0f;

    FirstPersonCamera(Camera cam, TerrainQuad terrain, Digga game) {
        this.cam = cam;
        sceneCam = new CameraNode("fpsCamera", cam);
        sceneCam.setControlDir(CameraControl.ControlDirection.SpatialToCamera);
        
        this.isEnabled = true;
        this.terrainToCheck = terrain;
        this.mainGame = game;
    }
    
    //Returns the XZ position of the character
    public Vector2f getPosition() {
        return new Vector2f(sceneCam.getWorldTranslation().getX(), sceneCam.getWorldTranslation().getZ());
    }
    
    public float getRotation() {
        return camPan;
    }
    
    public void setWalkSpeed(float speed) {
        walkSpeed = speed;
    }
    
    public void setRunSpeed(float speed) {
        runSpeed = speed;
    }
    
    public void setPlayerHeight(float height) {
        playerHeight = height;
    }
    
    public float getPlayerHeight() {
        return playerHeight;
    }
    
    public void setSensitivity(float sens) {
        sensitivity = sens;
    }
    
    public CameraNode getCameraNode() {
        return sceneCam;
    }
    
    public void warp(Vector3f position) {
        //Procedure for warping the player to a location
        sceneCam.setLocalTranslation(position);
    }

    public void registerWithInput(InputManager in, Node connect) {
        //Register inputs to use with the firstpersoncamera
        in.addMapping("moveForward", new KeyTrigger(KeyInput.KEY_W));
        in.addMapping("moveBack", new KeyTrigger(KeyInput.KEY_S));
        in.addMapping("moveLeft", new KeyTrigger(KeyInput.KEY_A));
        in.addMapping("moveRight", new KeyTrigger(KeyInput.KEY_D));
        in.addMapping("moveRun", new KeyTrigger(KeyInput.KEY_LSHIFT));
        // both mouse and button - rotation of cam
        in.addMapping("LookLeft", new MouseAxisTrigger(MouseInput.AXIS_X, true),
                new KeyTrigger(KeyInput.KEY_LEFT));

        in.addMapping("LookRight", new MouseAxisTrigger(MouseInput.AXIS_X, false),
                new KeyTrigger(KeyInput.KEY_RIGHT));

        in.addMapping("LookUp", new MouseAxisTrigger(MouseInput.AXIS_Y, false),
                new KeyTrigger(KeyInput.KEY_UP));

        in.addMapping("LookDown", new MouseAxisTrigger(MouseInput.AXIS_Y, true),
                new KeyTrigger(KeyInput.KEY_DOWN));
        
        in.addMapping("mainAction", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        in.addMapping("secondaryAction", new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
        
        //Register the mappings
        in.addListener(this, "moveForward", "moveBack", "moveLeft", "moveRight", "LookLeft", "LookRight", "LookUp", "LookDown");
        in.addListener(new ActionListener() {
            @Override
            public void onAction(String name, boolean keyPressed, float tpf) {
                if (name.equals("moveRun")) {
                    running = keyPressed;
                } else {
                    
                    if (!isEnabled || !mainGame.isConnected) return;
                    
                    //Mouse button clicked
                    CollisionResults results = new CollisionResults();
                    Ray ray = new Ray(sceneCam.getWorldTranslation(), cam.getDirection());
                    terrainToCheck.collideWith(ray, results);
                    
                    //Return if we don't find anything
                    if (results.size() == 0) return;
                    
                    Vector3f loc = results.getClosestCollision().getContactPoint();
                    
                    //Update the terrain to reflect our actions
                    if (name.equals("mainAction")) {
                        //Digging
                        //terrainToCheck.getControl(ClientTerrainModifier.class).digAtLocation(intersect);
                        mainGame.sendTerrainMessage(loc.getX(), loc.getZ(), true);
                    } else {
                        //Placing
                        //terrainToCheck.getControl(ClientTerrainModifier.class).depositAtLocation(intersect);
                        mainGame.sendTerrainMessage(loc.getX(), loc.getZ(), false);
                    }
                    
                }
            }
        }, "moveRun", "mainAction", "secondaryAction");
        connect.attachChild(sceneCam);
        
        input = in;
    }

    public void setEnabled(boolean active) {
        isEnabled = active;
        input.setCursorVisible(!isEnabled);
    }

    public boolean isEnabled() {
        return isEnabled;
    }
    
    public void checkCollide(Node nodeCheck) {
        CollisionResults result = new CollisionResults();
        CollisionResults resultUp = new CollisionResults();
        
        Ray rayDown = new Ray(sceneCam.getWorldTranslation(), new Vector3f(0, -1, 0));
        Ray rayUp = new Ray(sceneCam.getWorldTranslation(), new Vector3f(0, 1, 0));
        
        nodeCheck.collideWith(rayDown, result);
        nodeCheck.collideWith(rayUp, resultUp);

        if (result.size() > 0) {
            sceneCam.move(0, (-result.getClosestCollision().getDistance())+this.getPlayerHeight(), 0);
        } else if (resultUp.size() > 0) {
            System.out.println("[err] Character below world!");
            sceneCam.move(0, (resultUp.getClosestCollision().getDistance())+this.getPlayerHeight(), 0);
        }
    }

    @Override
    public void onAnalog(String name, float value, float tpf) {
        
        if (!isEnabled) return;
        
        float angles[] = new float[3];
        sceneCam.getWorldRotation().toAngles(angles);
        float angle = angles[1]; //Y angle
        
        float speed = running?runSpeed:walkSpeed;
        
        //Handle the movement of the camera
        if (name.equals("moveForward")) {
            sceneCam.move(((float) Math.sin(angle))*speed*tpf, 0, ((float) Math.cos(angle))*speed*tpf); //tan(ang) = z/x so z=xtan(ang)
        }
        if (name.equals("moveBack")) {
            sceneCam.move(((float) -Math.sin(angle))*speed*tpf, 0, ((float) -Math.cos(angle))*speed*tpf);
        }
        if (name.equals("moveLeft")) {
            sceneCam.move(((float) Math.sin(angle+Math.toRadians(90)))*speed*tpf, 0, ((float) Math.cos(angle+Math.toRadians(90)))*speed*tpf);
        }
        if (name.equals("moveRight")) {
            sceneCam.move(((float) Math.sin(angle-Math.toRadians(90)))*speed*tpf, 0, ((float) Math.cos(angle-Math.toRadians(90)))*speed*tpf);
        }
        
        this.checkCollide(terrainToCheck);
        
        //Mouselook
        switch (name) {
            case "LookLeft":
                camPan += value*sensitivity;
                break;
            case "LookRight":
                camPan -= value*sensitivity;
                break;
            case "LookUp":
                camTilt -= value*sensitivity;
                //sceneCam.rotate(-value*sensitivity, 0, 0);
                break;
            case "LookDown":
                camTilt += value*sensitivity;
                //sceneCam.rotate(value*sensitivity, 0, 0);
                break;
            default:
                break;
        }
        
        //Multiply by the final quaternions
        Quaternion finalRot = new Quaternion();
        finalRot.fromAngleAxis(camPan, Vector3f.UNIT_Y);
        finalRot.multLocal((new Quaternion()).fromAngleAxis(camTilt, Vector3f.UNIT_X));

        sceneCam.setLocalRotation(finalRot);
    }
    
}
